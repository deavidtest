/*
	Prueba de concepto de buscador de diferencias por bloques.
*/

#include <gcrypt.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
// Tipos de hashes :
// GCRY_MD_CRC32 - CRC32, sencillo, r�pido.
// GCRY_MD_SHA1 - SHA1 potente y seguro.
#define HASH_TYPE GCRY_MD_SHA1


// Establece cual es el ancho m�ximo de l�nea para trabajar.
// Establecer como m�nimo a 90. Recomendado 128 o 256.
#define MAX_LINE 1024
#define MAX_LOADED_LINES 8096

int block_size=15; // Valor recomendado: SQRT(min_lines).
// Versi�n 2: Multipasada en cascada: 63,31,15,7,3

unsigned int ihash(char *txt);
unsigned int *hash_loadfile(char *filename, int *size);

typedef struct structhashblock {
	int line1;
	int line2;
	int size;
} hashblock;

int main(int argc, char **argv) {
	// Comprobar la entrada de argumentos.
	if ( argc < 2 )	{
		fprintf( stderr, "Usage: %s <string>\n", argv[0] );
		fprintf( stderr, "Usage: %s --blr <base-filename> <local-filename> <remote-filename>\n", argv[0] );
		exit( 1 );
	}

	if (argc > 4 && strcmp(argv[1],"--blr")==0)
	{
		int *base_file=NULL, base_file_size=0;
		base_file=hash_loadfile(argv[2],&base_file_size);
		if (!base_file) exit( 0 ); // Error de memoria.
		fprintf( stderr, "Base File: %d lines loaded\n",base_file_size);

		int *local_file=NULL, local_file_size=0;
		local_file=hash_loadfile(argv[3],&local_file_size);
		if (!local_file) exit( 0 ); // Error de memoria.
		fprintf( stderr, "Local File: %d lines loaded\n",local_file_size);

		int *remote_file=NULL, remote_file_size=0;
		remote_file=hash_loadfile(argv[4],&remote_file_size);
		if (!remote_file) exit( 0 ); // Error de memoria.
		fprintf( stderr, "Remote File: %d lines loaded\n",remote_file_size);

		int line_base,line_local,size;
		int maxsize=1,total=0;
		int i,k,m;
		hashblock bloque[64];
		int nbloques=0;
		int lbb=0;
		int conf_pasada[]={256,128,64,32,16,8,4,2,1,0};
		int p;
		int min_bloque=0;
		for (p=0;min_bloque=conf_pasada[p];p++)
		{

			for (i=0;i<base_file_size;i+=maxsize) {
				maxsize=1;
				int j;
				for (j=0;j<nbloques;j++)
				{
					if (i>=bloque[j].line1 && i<=bloque[j].line1+bloque[j].size) break;
				}
				if (j<nbloques)
				{
					i=bloque[j].line1+bloque[j].size; continue;
				}


				for (k=0;k<local_file_size;k++)
				{
					int j;
					for (j=0;j<nbloques;j++)
					{
						if (k>=bloque[j].line2 && k<=bloque[j].line2+bloque[j].size) break;
					}
					if (j<nbloques)
					{
						k=bloque[j].line2+bloque[j].size; continue;
					}

					if (base_file[i]==local_file[k])
					{
						int nz=0,nzbl=0;

						for(m=0;k+m<local_file_size && i+m<base_file_size;m++)
						{
							for (j=0;j<nbloques;j++)
							{
								if (k+m>=bloque[j].line2 && k+m<=bloque[j].line2+bloque[j].size) break;
								if (i+m>=bloque[j].line1 && i+m<=bloque[j].line1+bloque[j].size) break;
							}
							if (j<nbloques) break;
							if (base_file[i+m]!=local_file[k+m])
							{
								nz++;
								if (nz>size/4) break;
								continue;
							}
							if (nz==0) size=m;
							else
							{
								nzbl++;
								if (nzbl>2)
								{
									nzbl=0;
									nz--;
								}

							}
						}

						if (size>maxsize)
						{
							maxsize=size;
							line_base=i;
							line_local=k;
						}
					}
				}

				if (maxsize>min_bloque)
				{
					if (nbloques<64)
					{
					bloque[nbloques].line1=line_base;
					bloque[nbloques].line2=line_local;
					bloque[nbloques].size=maxsize;
					nbloques++;
					} else printf( "Error: OUT OF BLOCKS. \n");


					printf("%d+%d:%d, ",maxsize,line_base-lbb,line_local-line_base);
	//				printf("BASE  - From: %d .. To: %d\n",line_base,line_base+maxsize);
		//			printf("LOCAL - From: %d .. To: %d\n",line_local,line_local+maxsize);
					lbb=line_base+maxsize;
					total+=maxsize;
				}

			}

		}
		printf("FIN.\n\n");

		int j;
		{
			hashblock auxbloque[64];
			int minline=0, min_j=0;
			for (p=0;p<nbloques;p++)
			{
				minline=base_file_size;
				for (j=0;j<64 && j<nbloques;j++)
				{
					if (bloque[j].line1<minline)
					{
						minline=bloque[j].line1;
						min_j=j;
					}
				}
				auxbloque[p]=bloque[min_j];
				bloque[min_j].line1=base_file_size;
			}
			memcpy(bloque,auxbloque,64*sizeof(hashblock));
		}
		lbb=0;
		int l2bb=0;
		int total_huerfanas=base_file_size-total;
		int total_huerfanas2=0;

		for (j=0;j<nbloques;j++)
		{
			total_huerfanas2+=bloque[j].line2-l2bb;
			printf("\t-- %d -> %d lineas huerfanas -- \n",bloque[j].line1-lbb, bloque[j].line2-l2bb);

			printf("BLOQUE %d:  @%d:%d \t--> \t%d:%d  \t(%d:%d)\n", j, bloque[j].line1,bloque[j].line2, bloque[j].line1+bloque[j].size,bloque[j].line2+bloque[j].size,bloque[j].line2-bloque[j].line1,bloque[j].size);

			lbb=bloque[j].line1+bloque[j].size+1;
			l2bb=bloque[j].line2+bloque[j].size+1;
		}
		if (base_file_size!=lbb+1 || local_file_size!=l2bb+1)
		{
			total_huerfanas2+=local_file_size-l2bb-1;
			printf("\t-- %d -> %d lineas huerfanas -- \n",base_file_size-lbb-1, local_file_size-l2bb-1);
		}
		float p100_found=100*total/(float)base_file_size;
		float p100_obsolete=100*(total_huerfanas)/(float)base_file_size;
		float p100_new=100*total_huerfanas2/(float)local_file_size;
		float p100_probability=100-p100_obsolete-p100_new;

		printf("\n\nRESUMEN: %d lineas encontradas en %d bloques (%.2f%% del total)\n",
						total,nbloques, p100_found);
		printf("\t(Total: %d lineas huerfanas. %.2f%% del fichero base es obsoleto)\n",	total_huerfanas,p100_obsolete);
		printf("\t(Total: %d lineas nuevas. %.2f%% del fichero local es nuevo)\n",total_huerfanas2,p100_new);
		printf(" -- %.2f%% de probabilidad de completar el merge correctamente -- \n",p100_probability);




		free(base_file);
		free(local_file);
		exit( 1 );
	}

	int i,b;
	int nhashes=argc-1;
	unsigned int *int_hash=malloc((nhashes)*sizeof(unsigned int));
	fprintf( stderr, "nhashes: %d\n",nhashes );
	for (i=1;i<argc;i++)
	{
		int_hash[i-1]=ihash(argv[i]);
	}

	for (i=0;i<nhashes-block_size+1;i++)
	{
		for (b=0;b<block_size;b++)
		{
			printf( "%08X", int_hash[i+b]);
		}
		printf( "\n");
	}
	free(int_hash);
	return 1;
}

void reducetext(char * txt) {

	int n=0, nn=0;
	char newline[256];
	char lastc=0;
	char c=txt[n];
	char type=0; // Tipos de palabras o grupos:
	// a -> texto, variable.
	// 1 -> n�meros, con, sin decimales.
	// % -> s�mbolos unarios, binarios.
	// 0 -> huecos y espacios

	for (n=0;n<MAX_LINE;n++) {
		c=tolower(txt[n]); // Captura del car�cter en min�scula.

		// Traducci�n del car�cter.
		switch(c)
		{
		 	// Retonos de carro y fin de fichero: salir de la funci�n.
			case 10:
			case 13:
			case 0:
				n=MAX_LINE; continue;
			// Tabuladores y espacios: cuentan como espacio.
			case ' ':
			case '\t':
				c=' '; break;
			// Acentos.
			case '�': c='a'; break;
			case '�': c='e'; break;
			case '�': c='i'; break;
			case '�': c='o'; break;
			case '�': c='u'; break;
		}

		switch(type) // Cambios de tipos seg�n algunos datos.
		{
			case 0: // Segun si est�bamos en un espacio.
				if (c>='0' && c<='9') {
					type='1';
				} else if (isalpha(c)) {
					type='a';
				} else type='%';
			break;
			case '1': // Segun si est�bamos en un espacio.
				if (c>='0' && c<='9') {
					type='1';
				} else if (c=='.') {
					type='1';
				} else if (isalpha(c)) {
					type='a';
				} else type='%';
			break;
			case 'a': // Segun si est�bamos en un espacio.
				if (c>='0' && c<='9') {
					type='a';
				} else if (isalpha(c)) {
					type='a';
				} else type='%';
			break;
			default:
			case '%':
				if (c==' ') {
					continue;
				} else
				if (c>='0' && c<='9') {
					type='1';
				} else if (isalpha(c)) {
					type='a';
				} else type='%';
			break;
		}
		if (c==' ') type=0;
		if (!nn && c==' ') continue; // Si est� tabulando al inicio, tampoco tiene efecto.
		if (c==lastc && (type=='a' || type==0)) continue; // Desperdiciar letras repetidas.

		if (type=='%' && newline[nn-1]==' ') nn--;
		newline[nn]=c;

		nn++;
		lastc=c;
	}
	newline[nn]=0;
	strcpy(txt,newline);

}

unsigned int *hash_loadfile(char *filename, int *size) {
	char *nombre=filename, linea[MAX_LINE];
	FILE *fichero;
	fichero = fopen( nombre, "r" );

	if( !fichero ) {
		printf( "Error (NO ABIERTO)\n" );
		return NULL;
	}
	char *txt;

	int lines;
	for (lines=0;fgets(linea, MAX_LINE, fichero); lines++);
	rewind(fichero);
	*size=lines;
	unsigned int *data=malloc(lines*sizeof(unsigned int*));
	int line=0;
	while (txt=fgets(linea, MAX_LINE, fichero)) {
		reducetext(txt);
		data[line++]=ihash(txt);
	}

	if( fclose(fichero)!=0 ) {
		printf( "\nError: fichero NO CERRADO\n" );
		return NULL;
	}

	return data;

}

// ihash calcula un hash de 32 bits para un texto.
unsigned int ihash(char *txt) {
	// Longitud del mensaje a cifrar
	int msg_len = strlen( txt );

	//  Longitud del hash resultante - gcry_md_get_algo_dlen
	// devuelve la longitud del resumen hash para un algoritmo
	int hash_len = gcry_md_get_algo_dlen( HASH_TYPE );

	// Salida del hash SHA1 - esto ser�n datos binarios
	unsigned char hash[ hash_len ];

	// Calcular el resumen SHA1. Esto es una especie de funci�n-atajo,
	// ya que la mayor�a de funciones gcrypt requieren
	// la creaci�n de un handle, etc.
	gcry_md_hash_buffer( HASH_TYPE, hash, txt, msg_len );

	//	unsigned int ihash=*((unsigned int *)hash);
	return *((unsigned int *)hash);
}
